package com.aplikasi.tubesss.optionmenu

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatDelegate
import com.aplikasi.tubesss.MenuHitungBDTR
import com.aplikasi.tubesss.R
import com.aplikasi.tubesss.WeatherApps

class SettingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkTheme()
        setContentView(R.layout.activity_setting2)
        var settingbtn = findViewById<Button>(R.id.settingbtn)
        var Aboutbtn = findViewById<Button>(R.id.aboutbtn)
        var helpbtn = findViewById<Button>(R.id.helpbtn)
        setTitle("OPTION")
        settingbtn.setOnClickListener(View.OnClickListener {
            val i = Intent(this, Setting::class.java)
            startActivity(i)
        })

         Aboutbtn.setOnClickListener(View.OnClickListener {
            val j = Intent(this, AboutActivity::class.java)
            startActivity(j)
        })

        helpbtn.setOnClickListener(View.OnClickListener {
            val j = Intent(this, HelpActivity::class.java)
            startActivity(j)
        })


    }
    private fun checkTheme() {
        when (MyThemStatus(this).darkMode) {
            0 -> {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                setTheme(R.style.Theme_Tubesss)
                delegate.applyDayNight()
            }
            1 -> {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                setTheme(R.style.Darktheme)
                delegate.applyDayNight()
            }
        }
    }
}


